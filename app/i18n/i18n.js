const availableLangs = ['id', 'en'];
const messages = {
    en: {
        "username": {
            'string.empty': `username cannot be an empty field`,
            'string.min': `username should have a minimum length of {#limit}`,
            'any.required': `username is a required field`,
        },
        "email": {
            'string.empty': `email cannot be an empty field`,
            'string.min': `email should have a minimum length of {#limit}`,
            'any.required': `email is a required field`,
        },
        "password": {
            'string.empty': `password cannot be an empty field`,
            'string.min': `password should have a minimum length of 6`,
            'any.required': `password is a required field`,
        },
        "password_confirmation": {
            'string.empty': `password_confirmation cannot be an empty field`,
            'any.only': `password_confirmation does not match`,
            'any.required': `password_confirmation is a required field`,
        },
        "success": {
            "register": `User was registered successfully!`,
        },
        "error": {
            "user": "User not found",
            "password": "Invalid password",
            "duplicateUser": "Username already exist",
            "duplicateEmail": "Email already exist",
            "role": "Role not found",
            "tokenNull": "No token provided!",
            "tokenFalse": "Unauthorizated"
        },
        "role": {
            "teacher": "required teacher role",
            "student": "required teacher role",
            "teacherOrStudent": "required teacher role",
            "admin": "required teacher role"
        },
        "exam": {
            "get": "Error cannot get exam",
            "name": "name cant be empty",
            "create": "error create exam"
        },
        "question": {
            "get": "Error cannot get quesiotn",
            "name": "name cant be empty",
            "create": "error create question",
            "deleteSuccess": "Question was deleted successfullly",
            "deleteError": "Cannot delete tutorial"
        }
    },
    id: {
        "username": {
            'string.empty': `username tidak boleh kosong`,
            'string.min': `minim karakter pada username 6`,
            'any.required': `username tidak ada`,
        },
        "email": {
            'string.empty': `email tidak boleh kosong`,
            'string.min': `minim karakter pada username 6`,
            'any.required': `email tidak ada`,
        },
        "password": {
            'string.empty': `password tidak boleh kosong`,
            'string.min': `minim karakter pada username 6`,
            'any.required': `password tidak ada`,
        },
        "password_confirmation": {
            'string.empty': `password_confirmation tidak boleh kosong`,
            'any.only': `minim karakter pada username 6`,
            'any.required': `password_confirmation tidak ada`,
        },
        "success": {
            "register": `Pendaftaran berhasil`,
        },
        "error": {
            "user": "User tidak ditemukan",
            "password": "pasword salah",
            "duplicateUser": "Username sudah digunakan",
            "duplicateEmail": "Email sudah digunakan",
            "role": "Role tidak ada",
            "tokenNull": "Token tidak ada",
            "tokenFalse": "Token salah"
        },
        "role": {
            "teacher": "kamu bukan guru",
            "student": "kamu bukan siswa",
            "teacherOrStudent": "kamu bukan guru atau siswa",
            "admin": "kamu bukan admin"
        },
        "exam": {
            "get": "Error tidak bisa mengambil exam",
            "name": "name tidak boleh kosong",
            "create": "error membuat exam"
        },
        "question": {
            "get": "Error mengambil question",
            "name": "name tidak boleh kosong",
            "create": "error membuat exam",
            "deleteSuccess": "Question berhasil dihapus",
            "deleteError": "Question tidak berhasil dihapus"
        }
    }
}

module.exports.availableLangs = availableLangs;
module.exports.messages = messages;