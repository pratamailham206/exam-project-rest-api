module.exports = (sequelize, Sequelize) => {
    const Exam = sequelize.define("exams", {
        name: {
            type: Sequelize.STRING,
        },
        desc: {
            type: Sequelize.STRING,
        },
        time: {
            type: Sequelize.INTEGER,
        }
    }, {
        // Create customized return settings
        scopes: {
            withoutTime: {
                attributes: { exclude: ['time'] },
            }
        }
    });

    return Exam;
}