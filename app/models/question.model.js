module.exports = (sequelize, Sequelize) => {
    const Question = sequelize.define("questions", {
        question: {
            type: Sequelize.STRING,
        },
        answer: {
            type: Sequelize.STRING,
        },
        published: {
            type: Sequelize.BOOLEAN,
            default: true,
        },
    }, {
        // Create customized return settings
        scopes: {
            withoutAnswer: {
                attributes: { exclude: ['answer'] },
            }
        }
    });

    return Question;
};