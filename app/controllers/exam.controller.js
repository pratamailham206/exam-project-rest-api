const db = require("../models");
const Exam = db.exams;
const Op = db.Sequelize.Op;

exports.getAll = (req, res) => {
    
    // Return Customized Attribute
    Exam.scope('withoutTime').findAll().then(data => {
        res.status(200).send(data);
    }).catch(err => {
        res.status(400).send({ message: req.polyglot.t(`exam.get`) })
    })
}

exports.create = (req, res) => {
    if(!req.body.name) {
        res.status(400).send({ message: req.polyglot.t(`exam.name`) });
        return;
    }
    Exam.create({
        name: req.body.name,
        desc: req.body.desc,
        time: req.body.time
    }).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(400).send({ message: req.polyglot.t(`exam.create`) })
    });
}