const db = require("../models");
const Question = db.questions;
const Op = db.Sequelize.Op;

exports.getAll = (req, res) => {
    
    // Return Customized attribute
    Question.scope('withoutAnswer').findAll().then(data => {
        res.status(200).send(data);
    }).catch(err => {
        res.status(400).send({ message: req.polyglot.t(`question.get`) })
    });
}

exports.create = (req, res) => {
    if(!req.body.question || !req.body.answer) {
        res.status(400).send({ message: req.polyglot.t(`question.name`) });
        return;
    }
    Question.create({
        question: req.body.question,
        answer: req.body.answer,
        published: req.body.published
    }).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(400).send({ message: req.polyglot.t(`question.create`) })
    });
}

exports.delete = (req, res) => {
    const id = req.params.id
    Question.destroy({ where: { id: id }}).then(num => {
        if (num == 1) {
            res.send({ message: req.polyglot.t(`question.deleteSuccess`) });
        } else {
            res.send({ message: req.polyglot.t(`question.deleteError`) });
        }
    }).catch(err => {
        res.status(500).send({ message: req.polyglot.t(`question.deleteError`) });
    });
}