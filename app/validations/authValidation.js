const Joi = require('joi');

const SignUpValidation = (data) => {
    const schema = Joi.object ({
        username: Joi.string()
                .min(2)
                .required()
                .label('username')

                // Joi custom return message
                .messages({
                    // return message customized with validation defined above
                    'string.empty': `{{#label}} cannot be an empty field`,
                    'string.min': `{{#label}} should have a minimum length of {#limit}`,
                    'any.required': `{{#label}} is a required field`,
                }),
        email: Joi.string()
                .min(6)
                .required()
                .email()
                .label('email')

                .messages({
                    'string.empty': `email cannot be an empty field`,
                    'string.min': `email should have a minimum length of {#limit}`,
                    'any.required': `email is a required field`,
                }),

        password: Joi.string()
                .min(6)
                .required()
                .label('password')
                .messages({
                    'string.empty': `password cannot be an empty field`,
                    'string.min': `password should have a minimum length of 6`,
                    'any.required': `password is a required field`,
                }),

        password_confirmation: Joi.any()
                .equal(Joi.ref('password'))
                .required()
                .label('password_confirmation')
                .options({ messages: { 'any.only': '{{#label}} does not match'} })
    });

    return schema.validate(data);
}

const SignInValidation = (data) => {
    const schema = Joi.object ({
        email: Joi.string()
                .min(6)
                .required()
                .email()
                .label('email')

                // Joi custom return message
                .messages({
                    // return message customized with validation defined
                    'string.empty': `email cannot be an empty field`,
                    'string.min': `email should have a minimum length of {#limit}`,
                    'any.required': `email is a required field`
                }),

        password: Joi.string()
                .min(6)
                .required()
                .label('password')
                .messages({
                    'string.empty': `password cannot be an empty field`,
                    'string.min': `password should have a minimum length of 6`,
                    'any.required': `password is a required field`
                }),
    });

    return schema.validate(data);
}

module.exports.SignInValidation = SignInValidation;
module.exports.SignUpValidation = SignUpValidation;