module.exports = app => {
    const exams = require("../controllers/exam.controller");
    const verify = require("../middlewares/authJwt");

    var router = require("express").Router();
    
    // Create Exam required teacher roles
    // Get Exam Required strudent and teacher roles
    router.get("/", [verify.verifyToken, verify.isStudentOrTeacher], exams.getAll);
    router.post("/", [verify.verifyToken, verify.isTeacher] ,exams.create);

    app.use('/api/exams' ,router);
}