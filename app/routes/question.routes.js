module.exports = app => {
    const questions = require("../controllers/question.controller");
    const verify = require("../middlewares/authJwt");

    var router = require("express").Router();

    // Create QUesion required teacher roles
    // Get Question required strudent and teacher roles
    // Delete Question required admin roles
    router.get("/", [verify.verifyToken, verify.isStudentOrTeacher], questions.getAll);
    router.post("/", [verify.verifyToken, verify.isTeacher] ,questions.create);
    router.delete("/:id", [verify.verifyToken, verify.isAdmin], questions.delete);

    app.use('/api/questions' ,router);
}