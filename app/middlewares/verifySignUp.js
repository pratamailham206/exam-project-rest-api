const db = require('../models');
const ROLES = db.ROLES;
const User = db.user;

const { SignUpValidation } = require('../validations/authValidation');

checkUsernameAndEmail = (req, res, next) => {
    // Joi validation
    const { error } = SignUpValidation(req.body);
    if (error) {
        errorLabel = error.details[0].context.label;
        errorType = error.details[0].type;
        return res.status(400).send({
            message: req.polyglot.t(`${errorLabel}.${errorType}`)
        });
    };

    User.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        if(user) {
            res.status(400).send({
                message: req.polyglot.t(`error.duplicateUser`)
            });
            return;
        }

        User.findOne({
            where: {
                email: req.body.email
            }
        }).then(user => {
            if(user) {
                res.status(400).send({
                    message: req.polyglot.t(`error.duplicateEmail`)
                });
                return;
            }
            next();
        })
    })
}

checkRolesExisted = (req, res, next) => {
    if (req.body.roles) {
        for (let i = 0; i < req.body.roles.length; i++) {
            if (!ROLES.includes(req.body.roles[i])) {
                res.status(400).send({
                    message: req.polyglot.t(`error.role`)
                });
                return;
            }
        }
    }
    next();
};

const verifySignUp = {
    checkUsernameAndEmail: checkUsernameAndEmail,
    checkRolesExisted: checkRolesExisted
};

module.exports = verifySignUp;