const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: req.polyglot.t(`error.tokenNull`)
    });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: req.polyglot.t(`error.tokenFalse`)
      });
    }
    req.userId = decoded.id;
    next();
  });
};

isStudent = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getRoles().then(roles => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "student") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: req.polyglot.t(`role.student`)
      });
      return;
    });
  });
};

isTeacher = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getRoles().then(roles => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "teacher") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: req.polyglot.t(`role.teacher`)
      });
    });
  });
};

isStudentOrTeacher = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getRoles().then(roles => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "student") {
          next();
          return;
        }

        if (roles[i].name === "teacher") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: req.polyglot.t(`role.teacherOrStudent`)
      });
    });
  });
};

isAdmin = (req, res, next) => {
    User.findByPk(req.userId).then(user => {
        user.getRoles().then(roles => {
            for (let i = 0; i < roles.length; i++) {
                if (roles[i].name === "admin") {
                    next();
                    return;
                }
            }
        
            res.status(403).send({
              message: req.polyglot.t(`role.admin`)
            });
        });
    });
}

const authJwt = {
  verifyToken: verifyToken,
  isStudent: isStudent,
  isTeacher: isTeacher,
  isStudentOrTeacher: isStudentOrTeacher,
  isAdmin: isAdmin
};
module.exports = authJwt;