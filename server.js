const express = require('express');
const cors = require('cors');
const createLocaleMiddleware = require('express-locale');
const app = express();
const { startPolyglot } = require('./app/utilities/startPolyglot');

require('dotenv').config();

var corsOptions = {
    origin: "http://localhost:8080"
};

// Middleware
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Get user locale
app.use(createLocaleMiddleware({
    "priority": ["accept-language", "default"],
    "default": "en_US"
}));
app.use(startPolyglot);

// DB Connect
const db = require("./app/models/index");
db.sequelize.sync();

app.get('/', (req, res) => {
    res.send({ message: "Welcom to Exam App server"});
});

//Routes
require("./app/routes/question.routes.js")(app);
require("./app/routes/exam.routes.js")(app);
require("./app/routes/auth.routes.js")(app);

// Start
app.listen(process.env.PORT, () => {
    console.log(`Server is running on port ${process.env.PORT}.`);
})